# About
This project provides a tool for duplicating one Backblaze b2 bucket to another Backblaze b2 bucket.  It originally was written to enable the quick migration of legacy b2 buckets to b2 buckets that are compatible with Amazon S3.  
The migration tool is written in Java making use of the official Backblaze b2 SDK.  It may be run as a stand-alone command-line program, or incorporated into another Java project.

## What's unique?
The migration tool makes use of the Backblaze b2 API calls to copy files without downloading from, OR uploading to, Backblaze b2.  It is therefore much faster to duplicate an existing, decently-sized bucket than trying to re-upload the entire thing from your computer to b2.

## Features
- Multi-threaded, allowing for configurable number of copy jobs to run simultaneously.
- Checks for existence of matching file (based on name and SHA1 checksum) at destination prior to copying, which avoids having unnecessary extra file versions
- Can be used as a standalone command-line program
- Logging uses SLF4j, so you can plug in your logger of choice.
- Default build has configurable logging [using a Groovy configuration file with Logback Classic](http://logback.qos.ch/manual/groovy.html) 
- Repository includes [IntelliJ](https://www.jetbrains.com/idea/) project files that can be used to get up and running fast

### Libraries used:
- [B2 Java SDK](https://github.com/Backblaze/b2-sdk-java)
- [Project Reactor](https://projectreactor.io/)
- [SLF4j](http://www.slf4j.org/)
- [JCommander](http://jcommander.org/) (for running as a stand-alone application)
- [Logback Classic](http://logback.qos.ch/) (as the default logger)

### System requirements:
- Java JDK v.1.8 or greater

# Usage:
## Environment Variables
Set the following environment variables: 
- B2_APPLICATION_KEY_ID
- B2_APPLICATION_KEY

## Pre-requisite steps to build the project (follow as needed)
### Install java
``sudo apt install default-jdk``

### Install gradle
See the official gradle instructions for installation: https://gradle.org/install/

### Clone the git Repository
For example:
``git clone https://gitlab.com/sasquatch-remix-friend/backblaze-b2-bucket-duplicator.git``

### Make the gradle wrapper executable
``chmod u+x ./backblaze-b2-bucket-duplicator/gradlew``

## Build the jar
From the root project directory:
``./gradlew shadowJar``

If everything goes well, the jar will be found under build/Libraries

## Usage
invoke from the command line:

``java -jar backblazemigration-1.0-SNAPSHOT-all.jar -s <sourceBucketName> -d <destinationBucketName>``

Both buckets must belong to the same account.

If the buckets are set to private, the applicationKey must have global permissions on the account (this is because Backblaze keys come in 2 flavors--keys specific to one bucket and keys account-wide).

By default, logging is only to the console.  Edit src/main/resources/logback.groovy to configure the logger level and/or add an appender to log to a file.

Known limitations:
- The user must set up all b2 buckets and keys before using migrator
- Only supported for "small" files.  [According to backblaze, "small" files are those under 5gb in size.](https://www.backblaze.com/b2/docs/b2_copy_file.html)
- "Large" file copying may be added in a future release.
- Requires Java 8 or later.  *Should* work on any system with Java.
