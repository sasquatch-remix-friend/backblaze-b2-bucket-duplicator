/*==============================================================================
 Copyright (c) 2020 Drew Maley / lawyertools.us
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 =============================================================================*/

package us.lawyertools.backblazemigration;


import com.backblaze.b2.client.B2ListFilesIterable;
import com.backblaze.b2.client.B2StorageClient;
import com.backblaze.b2.client.B2StorageClientFactory;
import com.backblaze.b2.client.exceptions.B2Exception;
import com.backblaze.b2.client.structures.B2Bucket;
import com.backblaze.b2.client.structures.B2CopyFileRequest;
import com.backblaze.b2.client.structures.B2FileVersion;
import com.backblaze.b2.client.structures.B2ListFileNamesRequest;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.util.concurrent.atomic.AtomicInteger;


public class B2Migrator
{

	public static final String USER_AGENT = "us.lawyertools.b2migrator/1.0-SNAPSHOT";

	private static final AtomicInteger totalCount = new AtomicInteger(0);
	private static final AtomicInteger skipCount  = new AtomicInteger(0);
	private static final AtomicInteger copyCount  = new AtomicInteger(0);

	public static final Logger logger = LoggerFactory.getLogger(B2Migrator.class);


	@Parameter(names = {"--source", "-s"}, required = true, description = "Source Bucket Name")
	public String sourceBucketName;

	@Parameter(names = {"--destination", "-d"}, required = true, description = "Target Bucket Name")
	public String targetBucketName;

	@Parameter(names = {"--parallelism", "-p"}, description = "Number of parallel 'rails' among which to distribute the migration requests")
	public Integer parallelism = 10;  // 10 is default value

	public static void main(String[] args)
	{
		B2Migrator migrator = new B2Migrator();

		try {
			JCommander.newBuilder()
					  .addObject(migrator)
					  .build()
					  .parse(args);

			B2StorageClient b2StorageClient = B2StorageClientFactory.createDefaultFactory().create(USER_AGENT);

			final B2Bucket sourceBucket = b2StorageClient.getBucketOrNullByName(migrator.sourceBucketName);
			final B2Bucket targetBucket = b2StorageClient.getBucketOrNullByName(migrator.targetBucketName);

			if(sourceBucket == null)
				B2Migrator.logger.error("Error:  Source bucket '{}' does not exist (or cannot be accessed using the supplied credentials)", migrator.sourceBucketName);

			else if(targetBucket == null)
					B2Migrator.logger.error("Error:  Source bucket '{}' does not exist (or cannot be accessed using the supplied credentials)", migrator.sourceBucketName);

			else {
				final B2Migrator b2Migrator = new B2Migrator();

				B2Migrator.logger.info("Migrating from {} to {} buckets.", sourceBucket.getBucketName(), targetBucket.getBucketName());

				b2Migrator.duplicateBucket(b2StorageClient, sourceBucket, targetBucket);
			}
		} catch(ParameterException e) {

			B2Migrator.logger.error(e.getLocalizedMessage());
			e.usage();
		} catch(B2Exception | RuntimeException b2Exception) {
			B2Migrator.logger.error(b2Exception.getLocalizedMessage(), b2Exception);
		} finally {
			Schedulers.shutdownNow();
			System.exit(0);
		}

	}

	// compare fileVersions by name and SHA1 hash.
	// Meant to compare fileVersions in different buckets to see if they match
	public static boolean fileMatch(B2FileVersion left, B2FileVersion right)
	{
		boolean matchName = left.getFileName().equalsIgnoreCase(right.getFileName());
		boolean matchSHA1 = left.getContentSha1().equalsIgnoreCase(right.getContentSha1());

		return matchName && matchSHA1;

	}

	public void duplicateBucket(B2StorageClient client, B2Bucket sourceBucket, B2Bucket targetBucket) throws B2Exception
	{
		B2ListFileNamesRequest listFileNamesRequest = B2ListFileNamesRequest.builder(sourceBucket.getBucketId())
																			//.setMaxFileCount(50)
																			.build();

		B2ListFilesIterable iterable = client.fileNames(listFileNamesRequest);//b2StorageClient.fileNames(sourceBucket.getBucketId());

		Flux.fromIterable(iterable)
			.parallel(this.parallelism)
			.runOn(Schedulers.elastic())

			.doOnNext((fileVersion) -> {

				try {
					int currentCount = B2Migrator.totalCount.incrementAndGet();
					if(currentCount % 500 == 0)
						B2Migrator.logger.info("Processed {} files", currentCount);

					//if(currentCount % 5000 == 0)
					//	System.out.println();

					if(fileVersion.isHide() == false && fileVersion.isFolder() == false && fileVersion.isStart() == false) {
						//B2ListFilesIterable response = client.fileNames(findFileRequest);

						B2FileVersion fileAtDestination = null;

						try {
							// getFileInfoByName uses an undocumented b2 api call 'get_file_info_by_name'
							// From observations we know that if the named file does not exist in the named bucket,
							// an error is thrown.  We test for existence of the file by trying to get its file info.
							// If an exception is thrown, and the code equals "not_found", it means the file does not yet exist
							// on the named bucket
							fileAtDestination = client.getFileInfoByName(targetBucket.getBucketName(), fileVersion.getFileName());
						} catch(B2Exception e) {
							if(!e.getCode().equals("not_found"))
								B2Migrator.logger.error(e.getLocalizedMessage(), e);


						}

						if(fileAtDestination == null || B2Migrator.fileMatch(fileVersion, fileAtDestination) == false) {

							try {
								B2CopyFileRequest copyFileRequest = B2CopyFileRequest.builder(fileVersion.getFileId(), fileVersion.getFileName())
																					 .setDestinationBucketId(targetBucket.getBucketId())
																					 .setMetadataDirective(B2CopyFileRequest.COPY_METADATA_DIRECTIVE)
																					 .build();
								B2FileVersion resultFileVersion = client.copySmallFile(copyFileRequest);
								B2Migrator.logger.debug("[{}] Copied filename: {}", B2Migrator.totalCount.get(), resultFileVersion.getFileName());
								B2Migrator.copyCount.incrementAndGet();
							} catch(B2Exception e) {
								B2Migrator.logger.error("[{}] filename: {}: encountered error when calling copySmallFile: {}",
														B2Migrator.totalCount.get(), fileVersion.getFileName(),
														e.getLocalizedMessage());
								System.out.println();
							}

						}

						else {
							B2Migrator.logger.debug("[file #{}] filename: {}: Skipping because we found an identical match in the destination bucket.", B2Migrator.totalCount.get(), fileAtDestination.getFileName());
							B2Migrator.skipCount.incrementAndGet();
						}

					}

				} catch(RuntimeException /*| B2Exception*/ b2Exception) {
					B2Migrator.logger.error("Encountered error trying to copy file \"{}\" [id: {}]", fileVersion.getFileName(), fileVersion.getFileId());
				}
			})
			.doOnTerminate(() -> B2Migrator.logger.info("Finished parallel thread " + Thread.currentThread().getName()))
			//.sequential()
			.then()
			.doOnTerminate(() -> B2Migrator.logger.info("Final count: [total: {}]  [skipped: {}]  [copied: {}]", totalCount.get(), skipCount.get(), copyCount.get()))
			.block();
	}
}

