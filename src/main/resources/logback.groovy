statusListener(OnConsoleStatusListener)

// mimic the default logger
appender("CONSOLE", ConsoleAppender) {

    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"
    }

}

//logger(B2Migrator.class.getName(), INFO, ["CONSOLE"])
root(INFO, ["CONSOLE"])
